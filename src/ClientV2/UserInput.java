package ClientV2;

import java.util.Scanner;

public class UserInput extends Thread {

    public UserInput() {
    }

    /**
     * this is the run-method of the user input thread.
     * in this tread the client prompts the user to input something and respond accordingly
     * for example with sending a message to the server, or quiting the client.
     */
    @Override
    public void run() {
        while (true) {

            printOptionMenu();

            System.out.print("> Input: ");

            Scanner sc = new Scanner(System.in);

            String userInput = sc.nextLine();

            while (!userInput.matches("[0-9]+")) {
                System.out.println("* Invalid number. Try again...");
                System.out.print("> Input: ");
                userInput = sc.nextLine();
            }

            boolean nrvalid = true;

            int input = Integer.parseInt(userInput);

            switch (input) {
                case 0:
                    Main.WriteToServer("QUIT");
                    break;

                case 1:
                    Main.WriteToServer("BCST " +
                            promptForAnInput("a Message", 2, 50, true, false));
                    break;

                case 2:
                    Main.WriteToServer("LOU");
                    break;

                case 3:
                    Main.WriteToServer("WSPR " +
                            promptForAnInput("the username of the receiver", 3, 14, true, true) + " " +
                            promptForAnInput("a Message", 2, 50, true, false));
                    break;

                case 4:
                    Main.WriteToServer("GRP_CRT " +
                            promptForAnInput("a name for the new chat-group", 5, 15, true, true));
                    break;

                case 5:
                    Main.WriteToServer("GRP_LS");
                    break;

                case 6:
                    Main.WriteToServer("GRP_JOIN " +
                            promptForAnInput("the name of a chat-group you want to join", 5, 15, true, true));
                    break;

                case 7:
                    Main.WriteToServer("GRP_MSG " +
                            promptForAnInput("the name of the target chat-group", 5, 15, true, true) + " " +
                            promptForAnInput("a Message", 2, 50, true, false));
                    break;

                case 8:
                    Main.WriteToServer("GRP_LEAV " +
                            promptForAnInput("the name of a chat-group you want to leave", 5, 15, true, true));
                    break;

                case 9:
                    Main.WriteToServer("GRP_KICK " +
                            promptForAnInput("the name of the chat-group you want to kick a user from", 5, 15, true, true) + " " +
                            promptForAnInput("the user you want to kick", 3, 14, true, true));
                    break;

                default:
                    System.out.println("Number not valid ...");
                    nrvalid = false;
            }

            if (nrvalid) {
                //ClientV2.Main.pause(1000);
                Main.EnterToConineu(false);
            }
        }
    }

    /**
     * this is a method to easily prompt the user for an input.
     * @param what a string with what this method needs to ask for.
     * @param minLength the minimal length of the input.
     * @param maxLength the maximum length of the input.
     * @param showMinMax if the method should show the boundaries for the length
     * @param specialCharsCheck if the method should check the input given by the user on only containing letter, numbers, and underscores.
     * @return the input given by the user.
     */
    public String promptForAnInput(String what, int minLength, int maxLength, boolean showMinMax, boolean specialCharsCheck) {

        if (showMinMax) {
            System.out.println("> Enter " + what + " ( " + minLength + " - " + maxLength + " ) ");
        } else {
            System.out.println("> Enter " + what);
        }

        Scanner sc = new Scanner(System.in);

        String returnvalue = "";


        boolean inputGood = false;
        while (!inputGood) {
            System.out.print("> Input: ");
            String input = sc.nextLine();

            if (specialCharsCheck) {
                if (input.matches("[a-zA-Z0-9_]*") && input.length() >= minLength && input.length() <= maxLength) {
                    returnvalue = input;
                    inputGood = true;
                } else {
                    System.out.println("* Input not valid. Try again: ");
                }
            } else {
                if (input.length() >= minLength && input.length() <= maxLength) {
                    returnvalue = input;
                    inputGood = true;
                } else {
                    System.out.println("* Input not valid. Try again: ");
                }
            }

        }

        return returnvalue;
    }

    /**
     * a method the prints the option menu.
     */
    private void printOptionMenu() {
        System.out.println("+-------------------< Available Options >--------------------+");
        System.out.println("| 1 - Send a message to all online users                     |");
        System.out.println("| 2 - Show a list of online users                            |");
        System.out.println("| 3 - Send a private message to another user (Whisper)       |");
        System.out.println("| 4 - Create a chat group                                    |");
        System.out.println("| 5 - Show a list of existing groups                         |");
        System.out.println("| 6 - Join a chat group                                      |");
        System.out.println("| 7 - Send a message to a chat group                         |");
        System.out.println("| 8 - Leave a chat group                                     |");
        System.out.println("| 9 - Kick another user from a chat group (Owner only)       |");
        System.out.println("+------------------------------------------------------------+");
        System.out.println("| 0 - Logout                                                 |");
        System.out.println("+------------------------------------------------------------+");
        System.out.println("> Enter a corresponding number");
    }
}
